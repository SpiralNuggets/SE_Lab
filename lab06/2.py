import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class DrawProgram(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.scene = QGraphicsScene(0, 0, 500, 500)

        self.view = QGraphicsView(self.scene, self)
        self.view.setGeometry(0, 0, 500, 500)

        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.view.show()

        self.view.setMouseTracking(True)
        self.view.mouseMoveEvent = self.mouseMoveEvent

        self.vertlayout = QVBoxLayout()
        self.vertlayout.addWidget(self.view)

        self.clearbutton = QPushButton("Clear")
        self.clearbutton.clicked.connect(self.clear)
        self.vertlayout.addWidget(self.clearbutton)

        self.setLayout(self.vertlayout)

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.scene.addEllipse(event.x(), event.y(),
                                  10, 10, QPen(), QBrush(Qt.black))

    def clear(self):
        self.scene.clear()


def main():
    app = QApplication(sys.argv)
    draw = DrawProgram()
    draw.show()
    return app.exec()


if __name__ == "__main__":
    sys.exit(main())
