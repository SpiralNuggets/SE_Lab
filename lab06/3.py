import turtle as t


class Disk(object):
    def __init__(self, name="", xpos=0, ypos=0, height=20, width=40):
        self.dname = name
        self.dxpos = xpos
        self.dypos = ypos
        self.dheight = height
        self.dwidth = width

    def showdisk(self):
        if t.heading() == 0:
            t.penup()
            t.goto(self.dxpos, self.dypos)
            t.pencolor("black")
            t.pendown()
            t.begin_fill()
            t.forward(self.dwidth/2)
            t.left(90)
            t.forward(self.dheight)
            t.left(90)
            t.forward(self.dwidth)
            t.left(90)
            t.forward(self.dheight)
            t.left(90)
            t.forward(self.dwidth/2)
            t.end_fill()

    def newpos(self, xpos, ypos):
        self.dxpos = xpos
        self.dypos = ypos

    def cleardisk(self):
        if t.heading() == 0:
            t.penup()
            t.goto(self.dxpos, self.dypos)
            t.pencolor("white")
            t.fillcolor("white")
            t.pendown()
            t.begin_fill()
            t.forward(self.dwidth/2)
            t.left(90)
            t.forward(self.dheight)
            t.left(90)
            t.forward(self.dwidth)
            t.left(90)
            t.forward(self.dheight)
            t.left(90)
            t.forward(self.dwidth/2)
            t.end_fill()


class Pole(object):
    def __init__(self, name="", xpos=0, ypos=0, thick=10, length=100):
        self.pname = name
        self.stack = []
        self.pxpos = xpos
        self.pypos = ypos
        self.pthick = thick
        self.plength = length

    def showpole(self):
        t.penup()
        t.goto(self.pxpos + self.pthick/2, self.pypos + self.pthick/2)
        t.pendown()
        for _ in range(2):
            t.left(90)
            t.forward(self.plength)
            t.left(90)
            t.forward(self.pthick)

    def pushdisk(self, disk):
        self.stack.append(disk)
        disk.newpos(self.pxpos, self.pypos + (len(self.stack)-1) * 20)
        disk.showdisk()

    def popdisk(self):
        disk = self.stack.pop()
        disk.cleardisk()
        return disk


class Hanoi(object):
    def __init__(self, n=3, start="A", workspace="B", desination="C"):
        self.startp = Pole(start, 0, 0)
        self.workspacep = Pole(workspace, 150, 0)
        self.destinationp = Pole(desination, 300, 0)
        self.startp.showpole()
        self.workspacep.showpole()
        self.destinationp.showpole()
        for i in range(n):
            self.startp.pushdisk(
                Disk("D" + str(i), 0, i * 150, 20, (n - i) * 30))

    def move_disk(self, start, destination):
        disk = start.popdisk()
        destination.pushdisk(disk)

    def move_tower(self, n, s, d, w):
        if n == 1:
            self.move_disk(s, d)
        else:
            self.move_tower(n - 1, s, w, d)
            self.move_disk(s, d)
            self.move_tower(n - 1, w, d, s)

    def solve(self):
        self.move_tower(3, self.startp, self.destinationp, self.workspacep)


t.speed(0)
h = Hanoi()
h.solve()

t.done()
