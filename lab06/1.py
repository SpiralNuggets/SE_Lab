import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Simple_drawing_window(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.setWindowTitle("Simple Github Drawing")
        self.rabbit = QPixmap("rabbit.png")

    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)

        p.setPen(QColor(0, 0, 0))
        p.setBrush(QColor(0, 127, 0))
        p.drawPolygon([
            QPoint(70, 100), QPoint(100, 110),
            QPoint(130, 100), QPoint(100, 150),
        ])

        p.setPen(QColor(255, 127, 0))
        p.setBrush(QColor(255, 127, 0))
        p.drawPie(50, 150, 100, 100, 0, 180 * 16)

        p.drawPolygon([
            QPoint(50, 200), QPoint(150, 200), QPoint(100, 400),
        ])

        p.drawPixmap(QRect(200, 100, 320, 320), self.rabbit)
        p.end()

class Simple_drawing_window3(Simple_drawing_window):
   def paintEvent(self, e):
        p = QPainter()
        p.begin(self)
        p.setPen(QColor(0, 0, 0))
        p.setBrush(QColor(0, 127, 0))
        p.drawEllipse(10, 10, 100, 100)
        p.end()

class Simple_drawing_window2(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.setWindowTitle("i screwed up windows so hard it doesnt remember what an exe is")
        self.picture = QPixmap("konplushbocw.png")
        
    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)

        p.setPen(QColor(0, 0, 0))
        p.setBrush(QColor(127, 127, 127))
        p.drawEllipse(50, 50, 100, 100)
        p.drawEllipse(150, 50, 100, 100)
        p.setBrush(QColor(92, 46, 15))
        p.drawRect(100,50,100,400)

        p.drawPixmap(QRect(400, 0, 320, 320), self.picture)

        p.end()

class Simple_drawing_window1(Simple_drawing_window):
    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)

        p.setPen(QColor(255, 127, 0))
        p.setBrush(QColor(255, 127, 0))

        p.setPen(QColor(255, 0, 0))
        p.setBrush(QColor(255, 0, 0))
        p.drawPolygon([
            QPoint(200, 200), QPoint(300, 200), QPoint(250, 300),
        ])

        p.end()

def main():
    app = QApplication(sys.argv)

    w = Simple_drawing_window()
    w1 = Simple_drawing_window1()
    w3 = Simple_drawing_window3()
    w.show()
    w1.show()
    w3.show()
    w2 = Simple_drawing_window2()
    w3 = Simple_drawing_window3()
    w.show()
    w1.show()
    w2.show()
    w3.show()

    return app.exec()


if __name__ == "__main__":
    sys.exit(main())
